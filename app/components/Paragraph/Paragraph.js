import React, {PropTypes} from 'react';
import {PARAGRAPH_TEXT} from '../../constants';

import './style.scss';

export default class Paragraph extends React.Component {

    static PropTypes = {
        type: PropTypes.number.isRequired,
        text: PropTypes.string,
        replayText: PropTypes.string
    };

    render() {
        let {replayText} = this.props;

        return (
            <div className="paragraph">
                {this.renderBody()}
                {replayText ?
                    <div className="paragraph__replay">{replayText}</div>
                    :
                    null
                }
            </div>
        )
    }

    renderBody() {
        let {
            type,
            text
        } = this.props;

        switch (type) {
            case PARAGRAPH_TEXT:
                return <pre>{text}</pre>
        }

        throw new Error('unknown paragraph type');
    }
}
