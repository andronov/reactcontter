import { PAGE_SET_TITLE } from '../constants';

const initialState = {
    title: 'PageTitle'
};


export default function page(state = initialState, action) {
    switch (action.type) {

        case PAGE_SET_TITLE:
            return {...state, title: action.title};
    }
    return state;
}