import React from 'react';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Header from '../Header';
import './style.scss';

@connect(
    state => ({page: state.page})
)
export default class Feed extends React.Component {

    render() {
        let { title } = this.props.page;
        console.log('FEED', this);
        return (
            <div className="feed layout">
                <Header params={this.props.params}></Header>
               
                <div className="layout__body">
                    {this.props.children}
                </div>
            </div>
        )
    }
}