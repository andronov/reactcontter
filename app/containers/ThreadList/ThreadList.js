import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import {List, ListItem, MakeSelectable} from 'material-ui/List';
import TextField from 'material-ui/TextField';

const SelectableList = MakeSelectable(List);


@connect(
    state => ({threads: state.threads})
)
export default class ThreadList extends React.Component {

    static PropTypes = {
        thread: PropTypes.object,
        onSelectThread: PropTypes.func
    };

    onSelectThread = (event, threadId) => {
        let {threads, onSelectThread} = this.props;
        if (onSelectThread) {
            onSelectThread(threads.find(t => t.id == threadId));
        }
    };

    render() {
        let {thread, threads} = this.props;
        return (
            <div>
                <div style={{padding: '1rem 1rem 0 1rem'}}>
                   
                </div>

                <SelectableList value={thread && thread.id} onChange={this.onSelectThread}>
                    {threads.map((record) => {
                        return <ListItem key={record.id}
                                         value={record.id}
                                         primaryText={record.name}/>
                    })}
                </SelectableList>
            </div>
        );
    }
}
