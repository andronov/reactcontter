import axios from 'axios';
//import  configureStore  from '../store/configureStore';
import { getWallsSuccess } from '../actions/wallActions';
import { getItemsSuccess } from '../actions/itemActions';
import  store  from '../store';
/**
 * Get all walls
 */

export function getWalls(username, slugwall) {
  return axios.post('http://127.0.0.1:8070/api/v1/wall/get/walls', {
  	username: username, 
  	slugwall: slugwall
  	})
    .then(response => {
      //configureStore.dispatch(getWallsSuccess(response.data.walls));
      store.dispatch(getWallsSuccess(response.data.walls));
      return response;
    });
}

/**
 * Get items from wall
 */

export function getItems(username, slugwall) {
  return axios.post('http://127.0.0.1:8070/api/v1/wall/get/items', {
  	username: username, 
  	slugwall: slugwall
  	})
    .then(response => {
      //configureStore.dispatch(getWallsSuccess(response.data.walls));
      store.dispatch(getItemsSuccess(response.data.items));
      return response;
    });
}