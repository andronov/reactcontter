
export const entryPoint = '/api';


export function resource(urlPath, config) {
    return fetch(entryPoint + urlPath, config)
        .then(response => response.json());
}