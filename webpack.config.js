const path = require('path');
const webpack = require('webpack');
const CleanPlugin = require('clean-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const merge = require('webpack-merge');

require('dotenv').config();

const development = require('./webpack/dev.config');
const production = require('./webpack/prod.config');

const TARGET = process.env.npm_lifecycle_event;

process.env.BABEL_ENV = TARGET;

const common = {
    entry: [
        'babel-polyfill',
        './app/index.js'
    ],

    output: {
        path: './dist',
        publicPath: '/'
    },

    resolve: {
        extensions: ['', '.jsx', '.js']
    },

    module: {
        preLoaders: [
            {test: /\.js$/, loader: 'eslint', exclude: /(node_modules)/}
        ],
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.(eot|ttf|woff|woff2|svg|svgz)$/,
                loader: 'file?name=fonts/[name].[ext]'
            }
        ]
    },

    postcss: () => {
        return [
            autoprefixer({
                browsers: ['last 2 versions']
            })
        ];
    },

    plugins: [
        new CleanPlugin(['dist']),
        new HtmlPlugin({
            template: 'app/index.html',
            inject: 'body'
        }),
        new CopyPlugin([
            {from: 'app/favicon.ico'}
        ])
    ]
};

if (TARGET === 'start' || !TARGET) {
    module.exports = merge(development, common);
}

if (TARGET === 'build' || !TARGET) {
    module.exports = merge(production, common);
}