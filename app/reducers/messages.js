import {MESSAGE_ADD, MESSAGE_BULK_ADD} from '../constants';

const initialState = [
    {
        id: 1,
        threadId: 1,
        authorId: 123123,
        createDate: 1442606006071,
        type: 'text',
        text: 'Old incoming message 90as d90a8s d09a8sd 90a90sd8 09a8d 09a8 d90as8 d09a8d90a 8d09ad8a09d 80ad89 0asd'
    },
    {
        id: 2,
        threadId: 1,
        authorId: 123123,
        createDate: 1442606006071 + 60 * 60 * 1000,
        type: 'text',
        text: 'New incoming message',
    }
];


export default function messages(state = initialState, action) {
    switch (action.type) {
        case MESSAGE_ADD:
            return [...state, action.message];

        case MESSAGE_BULK_ADD:
            return [...state, ...action.messages];

    }
    return state;
}
