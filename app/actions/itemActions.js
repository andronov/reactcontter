import * as types from '../actions/actionTypes';

export function getItemsSuccess(items) {
  return {
    type: types.GET_ITEMS_SUCCESS,
    items
  };
}