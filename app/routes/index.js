import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import Layout from '../containers/Layout/Layout';
import ChatPage from '../containers/ChatPage';
import Feed from '../containers/Feed/Feed';
import FeedItem from '../containers/FeedItem/FeedItem';
import LogInPage from '../containers/LogInPage';
// import auth from '../auth/authenticator';

export default function() {
    return (
        <Router history={browserHistory}>
            <Route path="/" component={Layout} onEnter={requireAuth}>
                <IndexRoute component={ChatPage}/>
            </Route>

            <Route path="/@:username/:slugwall" component={Feed} onEnter={requireAuth}>
                <IndexRoute component={FeedItem}/>
            </Route>
            <Route path="/login" component={LogInPage}/>
        </Router>
    );
}

function requireAuth(nextState, replace) {
  // console.log('req', auth.loggedIn());
  // if (!auth.loggedIn()) {
  //   replace({
  //     pathname: '/login',
  //     state: { nextPathname: nextState.location.pathname }
  //   })
  // }
}
