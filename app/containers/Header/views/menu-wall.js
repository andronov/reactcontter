import React from 'react';
import { Link } from 'react-router';

// Using "Stateless Functional Components"
export default function(props) {
  return (
    <div className="header-menu-wall">

      {props.walls.map(wall => {

        return (
          <div key={wall.id} className="header-menu-wall-item">
            <Link onClick={props.toggle.bind(null, props.params.username, wall.slug)} to={'/@'+props.params.username+'/' + wall.slug}>{wall.slug}</Link>
          </div>
        );

      })}

    </div>
  );
}
