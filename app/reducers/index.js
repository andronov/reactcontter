import { combineReducers } from 'redux';
import page from './page';
import threads from './threads';
import messages from './messages';
import walls from './walls';
import items from './items';

const rootReducer = combineReducers({
    page: page,
    threads: threads,
    messages: messages,
    walls: walls,
    items: items
});

export default rootReducer;
