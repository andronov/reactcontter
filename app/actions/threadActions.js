import {THREAD_SET_LIST} from '../constants';


export function setList(threads) {
    return {
        type: THREAD_SET_LIST,
        threads
    };
}
