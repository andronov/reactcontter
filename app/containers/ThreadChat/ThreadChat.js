import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createSelector } from 'reselect'
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentSend from 'material-ui/svg-icons/content/send';
import Message from '../../components/Message';
import Paragraph from '../../components/Paragraph';
import {PARAGRAPH_TEXT} from '../../constants';
import {getScrollBarWidth} from '../../services/Utils';
import {addMessage} from '../../actions/messageActions';

const CURRENT_USER_ID = 1; // Фейковый аутентификатор текущего авторизованного пользователя

/**
 * Селектор для формирования групп сообщений
 */
const messageGroupsSelector = createSelector(
    props => props.threadId,
    props => props.messages,
    (threadId, messages) => {
        let currentThreadMessages = messages.filter(m => m.threadId == threadId).sort((a, b) => a.createDate - b.createDate);
        let messageGroups = [], currentGroup = null, currentMessage;

        for (let i = 0; i < currentThreadMessages.length; i++) {
            currentMessage = currentThreadMessages[i];

            if (!currentGroup || currentGroup.authorId != currentMessage.authorId) {
                currentGroup = { id: currentMessage.id, authorId: currentMessage.authorId, messages: [] };
                messageGroups.push(currentGroup);
            }
            currentGroup.messages.push(currentMessage);
        }
        return messageGroups;
    }
);

let messageIdCounter = 1000;

import './style.scss';

@connect(
    state => ({messages: state.messages}),
    dispatch => ({
        messageActions: bindActionCreators({addMessage}, dispatch),
    })
)
export default class ThreadChat extends React.Component {

    static PropTypes = {
        threadId: PropTypes.number.isRequired,
        className: PropTypes.string
    };

    constructor() {
        super();

        this.needScrollDownMessages = true;
        this.state = {
            scrollBarWidth: getScrollBarWidth(),
            inputText: ''
        };
    }

    postInputText() {
        let {messageActions} = this.props;

        if (this.state.inputText && /\S/.test(this.state.inputText)) {
            messageActions.addMessage({
                id: messageIdCounter++,
                threadId: this.props.threadId,
                authorId: CURRENT_USER_ID,
                createDate: Date.now(),
                type: PARAGRAPH_TEXT,
                text: this.state.inputText
            });

            this.setState({inputText: ''});
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this.onWindowResize);
        this.messageScrollContainer.addEventListener('scroll', this.onMessagesScroll);

        this.scrollDownMessages();
        this.updateScrollPlaceholderHeight();

        // Интервал для вывода фейковых сообщений
        this.botIntervalId = setInterval(()=> {
            this.props.messageActions.addMessage({
                id: messageIdCounter++,
                threadId: this.props.threadId,
                authorId: 136786123,
                createDate: Date.now(),
                type: PARAGRAPH_TEXT,
                text: 'I a crazy chat bot. Nothing to stop me! ' + messageIdCounter
            });
        }, 5000);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onWindowResize);
        this.messageScrollContainer.removeEventListener('scroll', this.onMessagesScroll);

        clearInterval(this.botIntervalId);
    }

    componentDidUpdate(prevProps, prevState) {
        this.updateScrollPlaceholderHeight();

        if (this.state.inputText.replace(prevState.inputText, '') == '\n') {
            this.scrollDownMessages();
        }

        if (prevProps.messages != this.props.messages && this.needScrollDownMessages) {
            this.scrollDownMessages();
        }
    }

    onMessagesScroll = () => {
        if (!this.resizingTimeoutId) {
            // Этот флаг пригодится, когда будет необходимость выяснить
            // прокручивать ли сообщения вниз на этапе componentDidUpdateе
            this.needScrollDownMessages = this.isScrolledDownMessages();
        }
    };

    onWindowResize = () => {
        if (this.isScrolledDownMessages()) {
            // При растягивании экрана прокрутка ушла в низ
            this.needScrollDownMessages = true;
        }

        if (this.resizingTimeoutId) {
            clearTimeout(this.resizingTimeoutId);
        }
        this.resizingTimeoutId = setTimeout(this.onEndWindowResize, 500);

        // Если выставлен флаг - прокручиваем список сообщений в самый низ
        if (this.needScrollDownMessages) {
            this.scrollDownMessages();
        }
    };

    onEndWindowResize = () => {
        this.resizingTimeoutId = null;
    };

    scrollDownMessages() {
        this.messageScrollContainer.scrollTop = this.messageScrollContainer.scrollHeight;
    }

    updateScrollPlaceholderHeight() {
        // Заполняем пространство над списком сообщений
        this.scrollPlaceholder.style.height = Math.max(this.messageScrollContainer.clientHeight - this.messageContainer.clientHeight, 0) + 'px';
    }

    isScrolledDownMessages() {
        return ((this.messageScrollContainer.scrollTop + this.messageScrollContainer.clientHeight + 5) >= this.messageScrollContainer.scrollHeight);
    }

    onInputKeyDown = (event) => {
        if (event.keyCode == 13) {
            event.preventDefault();
            if (!event.ctrlKey) {
                this.postInputText();
            } else {
                this.setState({inputText: this.state.inputText + '\n'});
            }
        }
    };

    onInputChange = (event) => {
        this.setState({inputText: event.currentTarget.value});
    };

    onSendBtnTap = () => {
        this.postInputText();
    };

    setMessageScrollContainer = (el) => {
        this.messageScrollContainer = el;
    };

    setMessageContainer = (el) => {
        this.messageContainer = el;
    };

    setScrollPlaceholder = (el) => {
        this.scrollPlaceholder = el;
    };

    render() {
        let {inputText} = this.state;
        let {className} = this.props;

        return (
            <div className={'thread-chat ' + className}>
                hgfj
            </div>
        )
    }

    renderMessages() {
        let messageGroups = messageGroupsSelector(this.props);

        return (
            <div>
            </div>
        );
    }
}
