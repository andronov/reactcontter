import * as types from '../actions/actionTypes';

export function getWallsSuccess(walls) {
  return {
    type: types.GET_WALLS_SUCCESS,
    walls
  };
}

export function toggleWallsActive(slug) {
  return {
    type: types.TOGGLE_WALLS_ACTIVE,
    slug
  };
}