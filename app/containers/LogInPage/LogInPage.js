import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import AppBar from 'material-ui/AppBar';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as sessionActions from '../../actions/sessionActions';

import './style.scss';

class LogInPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {credentials: {}, step: 1, error: ''};

        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
    }

    onChange(event) {
        const field = event.target.name;
        const credentials = this.state.credentials;
        credentials[field] = event.target.value;
        return this.setState({credentials: credentials});
    }

    toRedirect(){
        this.props.history.pushState(null, '/');
    }

    onSave(event) {
        event.preventDefault();
        switch (this.state.step) {
            case 1:
                this.props.actions.loginUser(this);
                break;
            case 2:
                this.props.actions.verifyUser(this);
                break;
        }
    }

    render() {
        switch (this.state.step) {
            case 1:
                return (
                    <div className="login-form">
                        <Paper>
                            <AppBar
                                title="Login"
                                showMenuIconButton={false}/>
        
                            <form className="login-form__body" noValidate="noValidate">
                                <TextField
                                    name="phone"
                                    ref="phone"
                                    value={this.state.credentials.phone}
                                    onChange={this.onChange}
                                    floatingLabelText="Телефон"
                                    errorText={this.state.error}
                                    fullWidth={true}/>
        
                                <div className="login-form__submit">
                                    <RaisedButton onTouchTap={this.onSave} label="Login" primary={true} />
                                </div>
                            </form>
                        </Paper>
                    </div>
                );
            case 2:
                return (
                    <div className="login-form">
                        <Paper>
                            <AppBar
                                title="Login"
                                showMenuIconButton={false}/>
        
                            <form className="login-form__body" noValidate="noValidate">
                                <TextField
                                    name="sms_code"
                                    ref="sms_code"
                                    value={this.state.credentials.sms_code}
                                    onChange={this.onChange}
                                    floatingLabelText="Смс код"
                                    errorText={this.state.error}
                                    fullWidth={true}/>
        
                                <div className="login-form__submit">
                                    <RaisedButton onTouchTap={this.onSave} label="Login" primary={true} />
                                </div>
                            </form>
                        </Paper>
                    </div>
                );
        }
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(sessionActions, dispatch)
    };
}
export default connect(null, mapDispatchToProps)(LogInPage);
