import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {blueGrey500} from 'material-ui/styles/colors';
import configureStore from './store/configureStore';
import Routes from './routes';
import store from './store';

require('./styles/main.scss');

injectTapEventPlugin();

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: blueGrey500
    }
});

//const store = configureStore();
//const store = store;//configureStore();


ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider muiTheme={muiTheme}>
            <Routes/>
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('root')
);
