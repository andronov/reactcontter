import * as types from './actionTypes';
import sessionApi from '../services/SessionApi';
import auth from '../auth/authenticator';
import cookie from 'react-cookie';

export function loginVerify() {
  return {type: types.LOG_IN_VERIFY}
}

export function loginSuccess() {
  return {type: types.LOG_IN_SUCCESS}
}


export function loginUser(elem) {
  elem.state.credentials['device_id'] = auth.getDeviceId();
  return function(dispatch) {
    return sessionApi.login(elem.state.credentials).then(response => {
      var step = elem.state.step,
          error = '',
          errors = [],
          status = response.status;

      response.json().then(function(data) {
          for(var key in data){
            errors.push(data[key])
          }
    
          if(status == 201){step++;dispatch(loginVerify());}
          else{error = errors.join(', ')}
    
          elem.setState({
            step : step,
            credentials: elem.state.credentials,
            error: error
          });

          return data
      });
    }).catch(error => {
      throw(error);
    });
  };
}

export function verifyUser(elem) {
  elem.state.credentials['device_id'] = auth.getDeviceId();
  return function(dispatch) {
    return sessionApi.verify(elem.state.credentials).then(response => {
      var step = elem.state.step,
          error = '',
          errors = [],
          status = response.status;

      response.json().then(function(data) {
          for(var key in data){
            errors.push(data[key])
          }
    
          if(status == 202){dispatch(loginSuccess()); cookie.save('jwt', data.token); elem.toRedirect();return}
          else{error = errors.join(', ')}
    
          elem.setState({
            step : step,
            credentials: elem.state.credentials,
            error: error
          });
          return data
      });
    }).catch(error => {
      throw(error);
    });
  };
}


export function logOutUser() {
  auth.logOut();
  return {type: types.LOG_OUT}
}