import {AUTH_SERVER_LOGIN_URL, AUTH_SERVER_VERIFY_URL} from '../constants';

class SessionApi {
  static login(credentials) {
    const request = new Request(AUTH_SERVER_LOGIN_URL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(credentials)
    });


    return fetch(request).then(response => {
      return response;
    }).catch(error => {
      console.log(error)
      return error;
    });
  } 

  static verify(credentials) {
    const request = new Request(AUTH_SERVER_VERIFY_URL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }), 
      body: JSON.stringify(credentials)
    });


    return fetch(request).then(response => {
      return response;
    }).catch(error => {
      console.log(error)
      return error;
    });
  } 

}

export default SessionApi;