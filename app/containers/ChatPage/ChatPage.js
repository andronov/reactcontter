import React from 'react';
import { connect } from 'react-redux';
import Paper from 'material-ui/Paper';
import ThreadList from '../ThreadList';
import ThreadChat from '../ThreadChat';
import VisualItem from '../VisualItem';
import './style.scss';

@connect(
    state => ({}),
)
export default class ChatPage extends React.Component {

    constructor() {
        super();

        this.state = { thread: null }
    }

    onSelectThread = (thread) => {
        this.setState({thread});
    };

    render() {
        let {thread} = this.state;

        return (
            <div className="visual-page">
                
            </div>
            /*<div className="chat-page">
                <Paper className="chat-page__threads" zDepth={1}>
                    <ThreadList
                        thread={thread}
                        onSelectThread={this.onSelectThread}/>
                </Paper>

                {thread ?
                    <ThreadChat
                        className="chat-page__chat"
                        threadId={thread.id}/>
                    :
                    <div className="chat-page__empty">
                        <h4 className="mdl-typography--font-light">
                            Select chat
                        </h4>
                    </div>
                }
            </div>*/
        );
    }
}
