import { PAGE_SET_TITLE } from '../constants';


export function setTitle(title) {
    return {
        type: PAGE_SET_TITLE,
        title,
    }
}