import {MESSAGE_ADD, MESSAGE_BULK_ADD} from '../constants';


export function addMessage(message) {
    return {
        type: MESSAGE_ADD,
        message
    }
}


export function bulkAddMessage(messages) {
    return {
        type: MESSAGE_BULK_ADD,
        messages
    }
}
