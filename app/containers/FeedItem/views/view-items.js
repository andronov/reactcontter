import React from 'react';
import { Link } from 'react-router';
import Item from './view-item';

const Items = React.createClass({

  componentDidMount: function() {
  },

  render: function() {
    return (
      <div>
      {this.props.items.map(item => {

        return (
          <Item item={item}></Item>
        );

      })}
      </div>
    );
  }

});

export default Items;
/*
// Using "Stateless Functional Components"
export default function(props) {
  return (
    <div className="header-menu-wall">

      {props.walls.map(wall => {

        return (
          <div key={wall.id} className="header-menu-wall-item">
            <Link onClick={props.toggle.bind(null, props.params.username, wall.slug)} to={'/@'+props.params.username+'/' + wall.slug}>{wall.slug}</Link>
          </div>
        );

      })}

    </div>
  );
}
*/