import React, {PropTypes} from 'react';
import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import {blueGrey400} from 'material-ui/styles/colors';

import './style.scss';

export default class Message extends React.Component {

    static PropTypes = {
        my: PropTypes.bool,
        paragraphs: PropTypes.array.isRequired,
        childMessages: PropTypes.array
    };

    render() {
        let { my } = this.props;

        return !my ? this.renderIncomingMessage() : this.renderMyMessage();
    }

    renderIncomingMessage() {
        let {paragraphs} = this.props;

        return (
            <div className={'message'}>
                <Avatar
                    className="message__avatar" size={42}>A</Avatar>
                <Paper
                    className="message__body message__body--other">
                    {paragraphs}
                </Paper>
            </div>
        );
    }

    renderMyMessage() {
        let {paragraphs} = this.props;

        return (
            <div className={'message message--my'}>
                <Paper
                    style={{backgroundColor: blueGrey400, color: 'white'}}
                    className="message__body message__body--my">
                    {paragraphs}
                </Paper>
            </div>
        )
    }
}
