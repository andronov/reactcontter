export const PAGE_SET_TITLE = 'page_set_title';
export const AUTH_SERVER_LOGIN_URL = '/api/v1/auth/registration/';
export const AUTH_SERVER_VERIFY_URL = '/api/v1/auth/verify/sms/';

export const PARAGRAPH_TEXT = 'text';

export const THREAD_SET_LIST = 'thread_set_list';

export const MESSAGE_ADD = 'message_add';
export const MESSAGE_BULK_ADD = 'message_add_list';
