import uuid from 'uuid4';
import cookie from 'react-cookie';

class Auth {
    static loggedIn() {
        return !!cookie.load('jwt');
    }

    static logOut() {
        cookie.remove('jwt');
    }

    static getDeviceId() {
        if(!localStorage.device_id){
            localStorage.setItem('device_id', uuid());
            return uuid;
        }

        return localStorage.device_id;
    }

}

export default Auth;
