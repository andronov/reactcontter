const initialState = [
    {
        id: 1,
        name: 'Chat 1',
        messages: [
            {
                id: 1,
                authorId: 123123,
                createDate: 1442606006071,
                type: 'text',
                text: 'Hello'
            },
            {
                id: 2,
                authorId: 123123,
                createDate: 1442606006071 + 60 * 60 * 1000,
                type: 'text',
                text: 'My name is Bot'
            }
        ]
    },
    {
        id: 2,
        name: 'Chat 2',
        messages: []
    }
];


export default function threads(state = initialState) {
    return state;
}
