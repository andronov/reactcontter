export const LOG_IN_VERIFY = 'LOG_IN_VERIFY';
export const LOG_IN_SUCCESS = 'LOG_IN_SUCCESS';
export const LOG_OUT = 'LOG_OUT';
export const GET_WALLS_SUCCESS = 'GET_WALLS_SUCCESS';
export const TOGGLE_WALLS_ACTIVE = 'TOGGLE_WALLS_ACTIVE';
export const GET_ITEMS_SUCCESS = 'GET_ITEMS_SUCCESS';