import React from 'react';
import { connect } from 'react-redux';
import * as HttpApi from '../../services/HttpApi';
import './style.scss';
import { bindActionCreators } from 'redux'
import {toggleWallsActive} from '../../actions/wallActions';
import Items from './views/view-items';

const FeedItem = React.createClass({

    componentDidMount: function() {
        HttpApi.getItems(this.props.params.username, this.props.params.slugwall);
        console.log('this FeedItem', this);
    },

    componentDidUpdate: function(prevProps, prevState) {
        console.log('componentDidUpdate2', prevProps, prevState)
    },

    render: function() {
        console.log('this.props.items', this.props.items)
        return (
            <div className="feed-block">
                <Items items={this.props.items}></Items>
            </div>
        )
    }
})

const mapStateToProps = function(store) {
  return {
    items: store.items,
  };
};

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(toggleWallsActive, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(FeedItem);