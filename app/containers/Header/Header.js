import React from 'react';
import { connect } from 'react-redux';
import * as HttpApi from '../../services/HttpApi';
import HeaderWallMenu from './views/menu-wall';
import './style.scss';
import { bindActionCreators } from 'redux'
import {toggleWallsActive} from '../../actions/wallActions';
/*@connect(
    state => ({walls: {}}, console.log('s', state))
)*/
const Header = React.createClass({

    componentDidMount: function() {
        HttpApi.getWalls('admin', 'start');
        console.log('this', this);
    },

    componentDidUpdate: function(prevProps, prevState) {
        console.log('componentDidUpdate', prevProps, prevState)
    },

    toggleWall: function(username, slug){
        console.log('toggleWall', this, slug);
        HttpApi.getItems(username, slug);
        //this.props.actions.toggleWallsActive(slug)
    },

    render: function() {
        console.log('this.props.walls.walls', this.props.walls)
        return (
            <header className="header">

                       
            <HeaderWallMenu toggle={this.toggleWall} params={this.props.params} walls={this.props.walls} />


            </header>
        )
    }
})

const mapStateToProps = function(store) {
  console.log('sot', store);
  return {
    walls: store.walls.walls,
  };
};

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(toggleWallsActive, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);