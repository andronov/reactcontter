import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import {List, ListItem, MakeSelectable} from 'material-ui/List';
import TextField from 'material-ui/TextField';
import './style.css';
import {Layer, Rect, Stage, Group} from 'react-konva';
import Img from './Img.js';

const SelectableList = MakeSelectable(List);

var image = {
    "src": "https://dl.dropboxusercontent.com/u/139992952/multple/houses1.jpg",
    "width": 500,
    "height": 226
}
var cw,ch;
 var img=new Image();
img.crossOrigin='anonymous';
img.src="https://dl.dropboxusercontent.com/u/139992952/multple/houses1.jpg";

@connect(
    state => ({threads: state.threads})
)
export default class VisualItem extends React.Component {

    constructor() {
        super();
        this.state = {
            points: []
        };
    }

   /* static PropTypes = {
        thread: PropTypes.object,
        onSelectThread: PropTypes.func
    };

    onSelectThread = (event, threadId) => {
        let {threads, onSelectThread} = this.props;
        if (onSelectThread) {
            onSelectThread(threads.find(t => t.id == threadId));
        }
    };*/
    initImage(){
       /* var img=new Image();
        img.crossOrigin='anonymous';
        img.onload=start;
        img.src="https://dl.dropboxusercontent.com/u/139992952/multple/houses1.jpg";*/

        //this.props.img = img;

        function start(){

           // drawImage(0.25);// resize canvas to fit the img
            /*cw=canvas.width=img.width;
            ch=canvas.height=img.height;

            // draw the image at 25% opacity
            drawImage(0.25);

            // listen for mousedown and button clicks
            $('#canvas').mousedown(function(e){handleMouseDown(e);});
            $('#reset').click(function(){ points.length=0; drawImage(0.25); });*/
        }

    }

    drawImage(ctx, alpha){
        ctx.clearRect(0,0,cw,ch);
        ctx.globalAlpha=alpha;
        ctx.drawImage(img,0,0);
        ctx.globalAlpha=1.00;
    }
    handleMouseDown(e){
        var that = this;
        let cr = e.currentTarget;
        console.log("handleMouseDown", e, this);


        let offsetX = cr.content.offsetLeft;
        let offsetY = cr.content.offsetTop;
        let clientX = e.evt.clientX;
        let clientY = e.evt.clientY;

        console.log('e', offsetX, offsetY)


        // calculate mouseX & mouseY
        let mx=parseInt(clientX-offsetX);
        let my=parseInt(clientY-offsetY);

        let points = this.state.points;

        console.log('e', mx, my)

        points.push({x:mx,y:my});

        this.setState({
            points: points
        })

        var canvas = document.getElementsByTagName("canvas")[0];
        var ctx = canvas.getContext("2d");

        outlineIt();

        function outlineIt(){
            that.drawImage(ctx, 0.25)
            ctx.beginPath();
            ctx.moveTo(that.state.points[0].x,that.state.points[0].y);
            for(var i=0;i<that.state.points.length;i++){
                ctx.lineTo(that.state.points[i].x,that.state.points[i].y);
            }
            ctx.closePath();
            ctx.stroke();
            ctx.beginPath();
            //ctx.arc(that.state.points[0].x,that.state.points[0].y,10,0,Math.PI*2);
            ctx.closePath();
            ctx.stroke();
        }

        function clipIt(){

  // calculate the size of the user's clipping area
  var minX=10000;
  var minY=10000;
  var maxX=-10000;
  var maxY=-10000;
  for(var i=1;i<that.state.points.length;i++){
    var p=that.state.points[i];
    if(p.x<minX){minX=p.x;}
    if(p.y<minY){minY=p.y;}
    if(p.x>maxX){maxX=p.x;}
    if(p.y>maxY){maxY=p.y;}
  }
  var width=maxX-minX;
  var height=maxY-minY;



  // clip the image into the user's clipping area
  ctx.save();
  ctx.clearRect(0,0,cw,ch);
  ctx.beginPath();
  ctx.moveTo(that.state.points[0].x,that.state.points[0].y);
  for(var i=1;i<that.state.points.length;i++){
    var p=that.state.points[i];
    ctx.lineTo(that.state.points[i].x,that.state.points[i].y);
  }
  ctx.closePath();
  ctx.clip();
  ctx.drawImage(img,0,0);
  ctx.restore();

  // create a new canvas 
  var c=document.createElement('canvas');
  var cx=c.getContext('2d');
  console.log('c', c);

  // resize the new canvas to the size of the clipping area
  c.width=width;
  c.height=height;

  // draw the clipped image from the main canvas to the new canvas
  cx.drawImage(canvas, minX,minY,width,height, 0,0,width,height);

  // create a new Image() from the new canvas
  var clippedImage=new Image();
  clippedImage.onload=function(){
    // append the new image to the page
    document.body.appendChild(clippedImage);
  }
  clippedImage.src=c.toDataURL();

  console.log('clippedImage', clippedImage);


  // clear the previous points 
  that.state.points.length=0;
  that.setState({
            points: []
        })

  that.drawImage(ctx, 0.25)
           

  // redraw the image on the main canvas for further clipping
        }

        // push the clicked point to the points[] array
        //points.push({x:mx,y:my});

        // show the user an outline of their current clipping path
        //outlineIt();

        // if the user clicked back in the original circle
        // then complete the clip
        if(that.state.points.length>1){
            var dx=mx-that.state.points[0].x;
            var dy=my-that.state.points[0].y;
            if(dx*dx+dy*dy<10*10){
                clipIt();
            }
        }
    }

    componentDidMount() {
        //this.props.ctx = $('canvas')[0].getContext("2d");

        console.log('t', this);

        this.initImage();
    }

    render() {
       // let {thread, threads} = this.props;
       return (
          <Stage onMouseDown={this.handleMouseDown.bind(this)} ref="canvas" width={500} height={226}>
            <Layer>
            <Img src={image.src} width={image.width} height={image.height} space="fill"/>
            </Layer>
          </Stage>
        );
        /*return (
            <div>
               
            </div>
        );*/
    }
}